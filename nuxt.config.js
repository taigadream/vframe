module.exports = {
	/*
  ** Headers of the page
  */
	head: {
		title: 'Аэропорт «Шереметьево»',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{
				hid: 'description',
				name: 'description',
				content: 'SVO 3d representation'
			}
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }]
	},
	/*
  ** Customize the progress bar color
  */
	loading: { color: '#F9A02B' },
	/*
  ** Build configuration
  */
	mode: 'spa',
	css: [
		// '~assets/fonts/gilroy/gilroy.css',
		'~assets/fonts/roboto/Roboto.css',
		'~assets/styles/normalize.css'
	],
	routes: [{ path: '/', component: 'pages/index.vue' }],
	build: {
		vendor: ['axios', '~/vue.config.js'],
		postcss: [
			require('postcss-import')(),
			require('postcss-cssnext')(),
			require('postcss-color-alpha')(),
			require('postcss-color-function')()
		],
		/*
    ** Run ESLint on save
    */
		extend(config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				})
			}
		}
	}
}

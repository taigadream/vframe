import Vue from 'vue'
// Vue.config.productionTip = false
// Vue.config.performance = true

Vue.config.ignoredElements = [
	'a-scene',
	'a-assets',
	'a-asset-item',
	'a-sky',
	'a-camera',
	'a-text',
	'a-cursor',
	'a-entity',
	'a-box',
	'a-cylinder',
	'a-animation',
	'a-text'
]

import Vuex from 'vuex'
import mutations from './mutations'
import getters from './getters'

const store = () =>
	new Vuex.Store({
		state: {
			vrMode: false,
			cursor: 'a-grab-cursor',
			notification: false,
			notificationContent: ' ',
			panView: false,
			panSrc: '',
			skyColor: '#232833',
			zoomSpeed: 0.5,
			invertZoom: false,
			rotateSpeed: 0.05,
			objectTree: true,
			minDistance: 50,
			maxDistance: 200,
			maxPolarAngle: 90,
			minPolarAngle: 20,
			// maxPolarAngle: Math.PI / 2.1,
			showUI: true,
			cameraSettingsDialog: false,
			hasObjectsToInteract: false,
			interactionNode: null,
			inspector: false,
			enableDamping: true,
			hasTexture: true,
			currentScene: 'Airport',
			prevScene: 'Airport',
			climateSensorsVisible: false,
			climateSensors: [],
			airoportSceneMeshes: [
				// {
				//   name: String,
				//   hovered: Boolean
				// }
			],
			roomMeshes: [
				// {
				//   name: String,
				//   interactable: Boolean,
				//   meshState: {
				//     visible: Boolean,
				//     focused: Boolean,
				//     hovered: Boolean
				//   }
				// }
			],
			serverData: []
		},
		mutations,
		getters
	})

export default store

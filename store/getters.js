import _find from 'lodash/find'
import _filter from 'lodash/filter'

const getters = {
	getMeshByName: state => name => {
		return _find(state.roomMeshes, item => item.name === name)
	},
	getVisibilityList: state => {
		return state.roomMeshes.map(item => {
			return {
				name: item.name,
				visible: item.meshState.visible
			}
		})
	},
	getVisibilityByName: state => name => {
		const el = _find(state.roomMeshes, item => item.name === name)
		return el.meshState.visible
	},
	getFocusByName: state => name => {
		const el = _find(state.roomMeshes, item => item.name === name)
		return el ? el.meshState.focused : false
	},
	getFocusList: state => {
		const list = _filter(state.roomMeshes, item => item.interactable)

		return list.map(item => {
			return {
				name: item.name,
				focused: item.meshState.focused
			}
		})
	},
	getHoverByName: state => name => {
		const el = _find(state.roomMeshes, item => item.name === name)
		return el ? el.meshState.hovered : false
	},
	getHoverList: state => {
		const list = _filter(state.roomMeshes, item => item.interactable)

		return list.map(item => {
			return {
				name: item.name,
				hovered: item.meshState.hovered
			}
		})
	},
	getCurrentFocusName: state => {
		const el = _find(state.roomMeshes, item => item.meshState.focused === true)
		return el ? el.name : null
	},
	climateSensors: state => {
		const arr = _filter(
			state.serverData,
			item => item.mnemonic === 'ClimateSensor'
		)
		return arr || null
	}
}

export default getters

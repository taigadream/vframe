import _forEach from 'lodash/forEach'
import _find from 'lodash/find'

const mutations = {
	// UI
	toggleNotification: state => (state.notification = !state.notification),
	hideNotification: state => (state.notification = false),
	showNotification: state => (state.notification = true),
	setNotificationContent: (state, content) =>
		(state.notificationContent = content),

	toggleVR: state => (state.vrMode = !state.vrMode),
	toggleUI: state => (state.showUI = !state.showUI),
	toggleObjTree: state => (state.objectTree = !state.objectTree),

	setInspector: (state, bool) => (state.inspector = bool),

	// Настройки камеры
	toggleCameraSettingsDialog: state =>
		(state.cameraSettingsDialog = !state.cameraSettingsDialog),
	setZoomInvert: (state, bool) => (state.invertZoom = bool),
	setZoomSpeed: (state, n) => (state.zoomSpeed = n),
	setRotateSpeed: (state, n) => (state.rotateSpeed = n),
	setMaxPolarAngle: (state, n) => (state.maxPolarAngle = n),
	setMinPolarAngle: (state, n) => (state.minPolarAngle = n),
	setMinDistance: (state, n) => (state.minDistance = n),
	setMaxDistance: (state, n) => (state.maxDistance = n),

	// Обработка данных сервера
	addRoomObject: (state, obj) => state.roomMeshes.push(obj),
	addAirportSceneItem: (state, obj) => state.airoportSceneMeshes.push(obj),

	addShallowObject: (state, obj) => state.serverData.push(obj),
	extendObjectData: (state, { mnemonic, id, data }) => {
		let obj = _find(state.serverData, { mnemonic: mnemonic, id: id })
		obj.data = data
	},

	// Манипулирование сценой
	setInteractionState: (state, bool) => (state.hasObjectsToInteract = bool),
	getInteractionNode: (state, el) => (state.interactionNode = el),
	changeScene: (state, string) => (state.currentScene = string),
	setPrevScene: (state, string) => (state.prevScene = string),

	// Манипулирование объектами комнаты
	changeMeshState: (state, meshName, meshState, bool) => {
		let editableObject = state.roomMeshes.find(item => item.name === meshName)
		if (editableObject) editableObject[meshState] = bool
	},
	changeRoomVisibility: (state, bool) => {
		_forEach(state.roomMeshes, item => {
			item.meshState.visible = bool
		})
	},
	changeVisibilityByType: (state, resolver) => {
		let list = state.roomMeshes.filter(
			item => item.name.slice(0, -2) === resolver.type
		)
		if (list)
			_forEach(list, item => {
				item.meshState.visible = resolver.visible
			})
	},
	setFocusByName: (state, name) => {
		let el = state.roomMeshes.find(item => item.name === name)
		let others = state.roomMeshes.filter(item => item.name !== name)

		if (el) {
			el.meshState.focused = true
			_forEach(others, item => {
				item.meshState.focused = false
			})
		}
	},
	setBlurByName: (state, name) => {
		let el = state.roomMeshes.find(item => item.name === name)
		if (el) el.meshState.focused = false
	},
	setHoverByName: (state, name) => {
		let el = state.roomMeshes.find(item => item.name === name)
		let others = state.roomMeshes.filter(item => item.name !== name)

		if (el) {
			el.meshState.hovered = true
			_forEach(others, item => {
				item.meshState.hovered = false
			})
		}
	},
	setMouseLeaveByName: (state, name) => {
		let el = state.roomMeshes.find(item => item.name === name)
		if (el) el.meshState.hovered = false
	},

	// Манипулирование объектами сцены аэропорта
	setAirportSceneItemHover: (state, name) => {
		let el = state.airoportSceneMeshes.find(item => item.name === name)
		let others = state.airoportSceneMeshes.filter(item => item.name !== name)

		if (el) {
			el.hovered = true
			_forEach(others, item => {
				item.hovered = false
			})
		}
	},
	setAirportSceneItemLeave: (state, name) => {
		let el = state.airoportSceneMeshes.find(item => item.name === name)
		if (el) el.hovered = false
	},
	setPanSrc: (state, imgSrc) => (state.panSrc = imgSrc),
	setSkyColor: (state, color) => (state.skyColor = color),
	toggleSensors: state =>
		(state.climateSensorsVisible = !state.climateSensorsVisible),
	changeDamping: (state, bool) => (state.enableDamping = bool),
	changeTexture: (state, bool) => (state.hasTexture = bool)
}

export default mutations

if (typeof AFRAME === 'undefined') {
	throw new Error(
		'Component attempted to register before AFRAME was available.'
	)
}

// if (!AFRAME.components['log-area']) {
AFRAME.registerComponent('log-area', {
	multiple: false,
	clickHandler(e) {
		const target = e.detail.intersection.object
		console.log(target)
	},
	loadHandler(e) {
		e.target.addEventListener('click', this.clickHandler)
	},
	dependencies: ['raycaster'],
	init() {
		this.el.addEventListener('click', this.clickHandler)
	},
	remove() {
		this.el.removeEventListener('click', this.clickHandler)
	}
})
// }

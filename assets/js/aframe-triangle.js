if (typeof AFRAME === 'undefined') {
	throw new Error(
		'Component attempted to register before AFRAME was available.'
	)
}

// if (!AFRAME.geometries['triangle']) {
AFRAME.registerGeometry('triangle', {
	schema: {
		vertices: {
			default: ['-10 1 10', '-10 1 0', '10 1 0']
		}
	},
	multiple: false,
	init: function(data) {
		let geometry = new THREE.Geometry()
		geometry.vertices = data.vertices.map(vertex => {
			const points = vertex.split(' ').map(x => parseInt(x))
			return new THREE.Vector3(...points)
		})
		geometry.computeBoundingBox()
		geometry.faces.push(new THREE.Face3(0, 1, 2))
		geometry.mergeVertices()
		geometry.computeFaceNormals()
		geometry.computeVertexNormals()
		this.geometry = geometry
	}
})
// }
